﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomData : MonoBehaviour {

    [System.NonSerialized] public Text roomDataText;

    public string roomName = "";
    public int playerCount = 0;
    public int maxPlayer = 0;

    private void Awake()
    {
        roomDataText = GetComponentInChildren<Text>();
    }

    public void UpdateInfo()
    {
        roomDataText.text = string.Format(" {0} [{1}/{2}]"
            ,roomName, playerCount.ToString("0"), maxPlayer);
    }
}
//fd
