﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine.SceneManagement;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class PhotonInit : MonoBehaviourPunCallbacks {

    public enum ActivePanel
    {
        LOGIN = 0,
        ROOMS = 1,
        ROOMMAKE = 2
    }

    public ActivePanel activePanel = ActivePanel.LOGIN;

    private string gameVersion = "1.0";
    public string userId = "wonki";
    public byte maxPlayer = 4;

    public TMP_InputField txtUserID;
    public TMP_InputField txtRoomName;

    public GameObject[] panels;

    public GameObject room;
    public Transform gridTr;

    public Dropdown optionPN;

    void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Start ()
    {
        txtUserID.text = PlayerPrefs.GetString("USER_ID", "USER_" + Random.Range(1, 999));
        txtRoomName.text = PlayerPrefs.GetString("ROOM_NAME", "ROOM_" + Random.Range(1, 999));
        if(PhotonNetwork.IsConnected)
        {
            ChangePanel(ActivePanel.ROOMS);
        }
	}

    #region SELF_CALLBACK_FUNCTION
    //panel1
    public void OnLoginClick()
    {
        PhotonNetwork.GameVersion = gameVersion;
        PhotonNetwork.NickName = txtUserID.text;

        PhotonNetwork.ConnectUsingSettings();

        PlayerPrefs.SetString("USER_ID",PhotonNetwork.NickName);

        ChangePanel(ActivePanel.ROOMS);
    }

    //panel2
    public void OnXCLick()
    {
        ChangePanel(ActivePanel.LOGIN);
    }

    public void OnMakeRoomPanelClick()
    {
        ChangePanel(ActivePanel.ROOMMAKE);
    }

    //panel3
    public void OnCreateRoomClick()
    {
        byte opt = 4;
        switch (optionPN.value)
        {
            case 0:
                opt = 4; break;
            case 1:
                opt = 8; break;
            default:
                opt = 4; break;
        }

        PhotonNetwork.CreateRoom(txtRoomName.text,
            new RoomOptions { MaxPlayers = opt });
    }
    public void OnCancelClick()
    {
        ChangePanel(ActivePanel.ROOMS);
    }


    //public void OnJoinRandomRoomClick()
    //{
    //    PhotonNetwork.JoinRandomRoom();
    //}

    #endregion


    #region PHOTON_CALLBACK_FUNCTION
    private void ChangePanel(ActivePanel panel)
    {
        foreach(GameObject _panel in panels)
        {
            _panel.SetActive(false);
        }

        panels[(int)panel].SetActive(true);
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connect To Master");
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("Joined Lobby");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Failed Join RandomRoom.");   
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Joined Room");
        SceneManager.LoadScene("2.Room");
        
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < gridTr.childCount; i++)
        {
            Destroy(gridTr.GetChild(i).gameObject);
        }

        foreach (RoomInfo roomInfo in roomList)
        {
            GameObject _room = Instantiate(room, gridTr);
            RoomData roomData = _room.GetComponent<RoomData>();
            roomData.roomName = roomInfo.Name;
            roomData.maxPlayer = roomInfo.MaxPlayers;
            roomData.playerCount = roomInfo.PlayerCount;
            roomData.UpdateInfo();
            roomData.GetComponent<Button>().onClick.AddListener
            (
                delegate
                {
                    OnClickRoom(roomData.roomName);
                }
           );
        }
    }
    #endregion


    void OnClickRoom(string roomName)
    {
        PhotonNetwork.NickName = txtUserID.text;
        PhotonNetwork.JoinRoom(roomName, null);
        PlayerPrefs.SetString("USER_ID", PhotonNetwork.NickName);
    }
}
