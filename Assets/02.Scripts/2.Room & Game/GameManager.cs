﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Voice.Unity;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static bool isEsc = false;
    public static Recorder recorder;

    public static bool isInfest = false;
    public static GameObject myPlayer;

    public static int sceneState = 0;
        

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(this);
        recorder = GameObject.Find("Voice").GetComponent<Recorder>();
        CreatePlayer();
    }

    

    // Update is called once per frame
    void Update ()
    {
        ESCToggle();
    }

    void ESCToggle()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isEsc = !isEsc;
        }
    }

    void CreatePlayer()
    {
        //Transform[] points =
        //    GameObject.Find("SpawnPointGroup").GetComponentsInChildren<Transform>();
        //int idx = Random.Range(1, points.Length);
        GameObject player = PhotonNetwork.Instantiate("Player",
            new Vector3(0, 2, 0), Quaternion.identity);
    }
}
