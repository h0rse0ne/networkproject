﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Voice;
using Photon.Voice.PUN;
using UnityStandardAssets.Utility;
using TMPro;

public class PlayerCtrl : MonoBehaviourPunCallbacks, IPunObservable
{ 
    public Animator anim;

    CharacterController control;

    float speedRun = 5;
    float speedJump = 8;
    float gravity = 16;

    Vector3 moveDir;
    float accel = 1;
    bool canMove = true;

    float mouseSpeed = 90;
    private Vector3 rot;

    bool isEsc;

    public bool InfectedPlayer;  //게임 시작 시 플레이어가 일반인인지 감염자인지 결정
    public bool OnInfected;                //감염자 모드 실행
    public lightMapManager Night;              // 밤

    Transform voterPos;
    Transform executionPos;
    public GameObject originPos;

    bool BeAttacked = false;

    //Wonki
    private Vector3 currPos;
    private Quaternion currRot;

    private Transform tr;
    private PhotonVoiceView photonVoiceView;

    public TextMeshProUGUI nickName;
    public SpriteRenderer micImage;
    public GameObject privateCam;

    private bool Infest = false;

    private float sendSpeed;
    private float sendDirection;
    private bool sendHit;
    private bool sendPunch;
    private bool sendJump;

    private float currSpeed;
    private float currDirection;
    private bool currHit;
    private bool currPunch;
    private bool currJump;

    // Use this for initialization
    void Awake()
    {
        DontDestroyOnLoad(this);
        control = GetComponent<CharacterController>();
    }

    private void Start()
    {
        tr = GetComponent<Transform>();
        photonVoiceView = GetComponent<PhotonVoiceView>();
        GetComponent<AudioSource>().volume = 10.0f;
        nickName.text = photonView.Owner.NickName;
        GameManager.myPlayer = this.gameObject;

        if (photonView.IsMine)
        {
            if (GameManager.sceneState == 3)
            {
                Night = GameObject.Find("LightMapManager").GetComponent<lightMapManager>();
                voterPos = GameObject.Find("voterPosition").transform;
                executionPos = GameObject.Find("executionPosition").transform;
                originPos = Resources.Load("originPostion") as GameObject;
            }
        }
        else
        {
            privateCam.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        micImage.enabled = photonVoiceView.IsSpeaking;

        if (photonView.IsMine)
        {
            ESCMenu();
            MovePlayer();
            if (GameManager.sceneState == 3)
            {
                InfectedModState();
                //GotoVote();
            }
            if (isEsc) return;
            InputMouse();
            InputKeys();
        }
        else
        {
            UpdateOther();
        }
    }

    void UpdateOther()
    {
        if ((tr.position - currPos).sqrMagnitude >= 10.0f * 10.0f)
        {
            tr.position = currPos;
            tr.rotation = currRot;
        }
        else
        {
            tr.position = Vector3.Lerp(tr.position, currPos, Time.deltaTime * 10.0f);
            tr.rotation = Quaternion.Slerp(tr.rotation, currRot, Time.deltaTime * 10.0f);
        }

        //Animation 동기화
        anim.SetFloat("Speed",currSpeed);
        anim.SetFloat("Direction",currDirection);
        if (currJump)
        {
            anim.SetTrigger("isJump");
            currJump = false;
        }
        if (currHit)
        {
            anim.SetTrigger("Hit");
            currHit = false;
        }
        if (currPunch)
        {
            anim.SetTrigger("isPunch");
            currPunch = false;
        }
    }

    public void SetInfest()
    {
        Infest = true;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(tr.position);
            stream.SendNext(tr.rotation);
            stream.SendNext(sendSpeed);
            stream.SendNext(sendDirection);
            stream.SendNext(sendJump);
            sendJump = false;
            stream.SendNext(sendHit);
            sendHit = false;
            stream.SendNext(sendPunch);
            sendPunch = false;
        }
        else
        {
            currPos = (Vector3)stream.ReceiveNext();
            currRot = (Quaternion)stream.ReceiveNext();
            currSpeed = (float)stream.ReceiveNext();
            currDirection = (float)stream.ReceiveNext();
            currJump = (bool)stream.ReceiveNext();
            currHit = (bool)stream.ReceiveNext();
            currPunch = (bool)stream.ReceiveNext();
        }
    }

    //입력
    private void InputKeys()
    {
        float v = sendSpeed = Input.GetAxis("Vertical"); // -1.0f ~ 0.0f ~ +1.0f
        float h = sendDirection = Input.GetAxis("Horizontal"); // -1.0f ~ 0.0f ~ +1.0f
        anim.SetFloat("Speed", v);
        anim.SetFloat("Direction", h);
        

        moveDir.z = Input.GetAxis("Vertical");
        moveDir.x = Input.GetAxis("Horizontal");
        
        //점프
        bool bJump = control.isGrounded && Input.GetButtonDown("Jump"); /*|| Input.GetButtonDown("OVRJump")*/   //isGrounded 땅에 있는지 검출
        if (bJump)
        {
            moveDir.y = speedJump * accel;
            anim.SetTrigger("isJump");
            sendJump = true;
        }

        if (InfectedPlayer)  //감염자 플레이어가
        {
            if (Night.night) //밤에
            {
                if (!OnInfected && Input.GetKeyDown(KeyCode.LeftShift)) //변신 상태가 아닐때 쉬프트를 누르면
                {
                    OnInfected = !OnInfected;           //변신모드
                }
                else if (OnInfected && Input.GetKeyDown(KeyCode.LeftShift))
                {
                    OnInfected = !OnInfected;
                }
            }
            else
            {
                OnInfected = false;
            }
        }
    }


    private void InputMouse()
    {
        //상하 회전
        rot.x -= Input.GetAxis("Mouse Y") * mouseSpeed * Time.deltaTime;
        rot.x = Mathf.Clamp(rot.x, -90, 90);
        Camera.main.transform.localRotation = Quaternion.Euler(rot.x, 0f, 0f);

        //좌우 회전
        rot.y = Input.GetAxis("Mouse X") * mouseSpeed * Time.deltaTime;
        transform.Rotate(0f, rot.y, 0f);

        //때리는 애니메이션
        if (Input.GetMouseButton(0) && !GameManager.isEsc)
        {
            anim.SetTrigger("Hit");
            sendHit = true;
        }
    }
    

    //이동
    private void MovePlayer()
    {
        moveDir.z = (canMove ? moveDir.z : 0) * speedRun * accel;
        moveDir.x = (canMove ? moveDir.x : 0) * speedRun * accel;


        //중력 중요**
        moveDir.y -= gravity * Time.deltaTime;
        if (control.isGrounded && moveDir.y < -1)
        {
            moveDir.y = -1;
        }
        Vector3 forword = transform.TransformDirection(moveDir);
        control.Move(forword * Time.deltaTime);
    }

    public void ESCMenu()
    {
        if (GameManager.isEsc)
        {
            moveDir.z = 0;
            moveDir.x = 0;
        }
    }

    void InfectedModState() //감염자로 되었을 때 능력치
    {
        if (OnInfected) { accel = 1.5f; }
        else accel = 1f;
    }

    private void _OnControllerColliderHit(ControllerColliderHit hit) //캐릭터 컨트롤러 충돌 판정 함수
    {

    }

    private void OnTriggerEnter(Collider other)
    {

    }

    void GotoVote() //테스트 코드
    {
        if (Input.GetButtonDown("Fire1"))
        {
            originPos.transform.position = this.transform.position;
            this.transform.position = voterPos.transform.position;
        }
        else if (Input.GetButtonDown("Fire2"))
        {
            this.transform.position = originPos.transform.position;
            //this.transform.position = voterPos.transform.position;
        }

    }

    void GoToExecutor()
    {
        originPos.transform.position = this.transform.position;
        if (!Night && BeAttacked)
        {  
            this.transform.position = executionPos.transform.position;
        }
        else
        {
            this.transform.position = voterPos.transform.position;
        }
    }

    void ExitFromExecutor()
    {
        this.transform.position = originPos.transform.position;
    }
}