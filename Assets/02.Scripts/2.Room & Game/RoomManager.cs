﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RoomManager : MonoBehaviourPunCallbacks
{

    public TextMeshProUGUI playerCountText;
    public GameObject escapePanel;
    public TextMeshProUGUI CountDownText;

    [SerializeField] private bool isReady = false;
    [SerializeField] private bool isCountDown = false;

    private Coroutine cor_countdown;

    private void Awake()
    {
        GameManager.sceneState = 2;
    }

    // Use this for initialization
    void Start ()
    {
        // 커서 초기화 및 몇가지 변수 초기화
        GameManager.isEsc = false;  
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        isReady = false;
        isCountDown = false;
        CountDownText.gameObject.SetActive(false);

        CheckPlayerCount();
	}

    private void Update()
    {
        checkESC();
        CheckReady();
    }

    public void checkESC()
    {
        if (GameManager.isEsc == false)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            escapePanel.SetActive(false);
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            escapePanel.SetActive(true);
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        CheckPlayerCount();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        CheckPlayerCount();
    }

    public override void OnLeftRoom()
    {
        
        Destroy(GameObject.Find("Voice"));
        Destroy(GameObject.Find("GameManager"));
        foreach(GameObject obj in GameObject.FindGameObjectsWithTag("PLAYER"))
        {
            Destroy(obj);
        }

        SceneManager.LoadScene("1.Lobby");
    }

    public void CheckPlayerCount()
    {
        int currPlayer = PhotonNetwork.PlayerList.Length;
        int maxPlayer = PhotonNetwork.CurrentRoom.MaxPlayers;
        playerCountText.text = string.Format("[{0}/{1}]", currPlayer, maxPlayer);

        if (currPlayer == maxPlayer)
        {
            isReady = true;
        }
        else
        {
            isReady = false;
        }
    }

    public void CheckReady()
    {
        if(isReady && isCountDown == false)
        {
            cor_countdown = StartCoroutine(CountDown());
        }
    }

    IEnumerator CountDown()
    {
        int count = 10;
        isCountDown = true;
        CountDownText.gameObject.SetActive(true);

        while (count != 0)
        {
            count--;
            CountDownText.text = string.Format("{0}",count);
            yield return new WaitForSeconds(1.0f);

            if (!isReady)
            {
                isCountDown = false;
                CountDownText.gameObject.SetActive(false);
                yield break;
            }
        }

        CountDownText.gameObject.SetActive(false);
        SceneManager.LoadScene("BaseScene");
    }

    public void yesClick()
    {
        isReady = false;
        isCountDown = false;
        //StopCoroutine(cor_countdown);
        PhotonNetwork.LeaveRoom();
    }

    public void noClick()
    {
        GameManager.isEsc = false;
    }
}
