﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Voice;
using Photon.Voice.PUN;
using UnityStandardAssets.Utility;
using TMPro;

public class PlayerInit : MonoBehaviourPunCallbacks, IPunObservable
{
    public TextMeshProUGUI nickName;
    public SpriteRenderer micImage;
    public GameObject privateCam;
    public Animator anim;

    private Transform tr;
    private PhotonVoiceView photonVoiceView;

    private Vector3 currPos;
    private Quaternion currRot;

    private float currSpeed;
    private float currDirection;
    private bool currHit;
    private bool currPunch;
    private bool currJump;

    private bool Infest = false;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start ()
    {
        tr = GetComponent<Transform>();
        photonVoiceView = GetComponent<PhotonVoiceView>(); 
        GetComponent<AudioSource>().volume = 10.0f;
        nickName.text = photonView.Owner.NickName;

        if (photonView.IsMine)
        {
            GameManager.myPlayer = this.gameObject;
        }
        else
        {
            privateCam.SetActive(false);
        }
    }
	
	
	void Update ()
    {
        if (!photonView.IsMine)
        {
            if ((tr.position - currPos).sqrMagnitude >= 10.0f * 10.0f)
            {
                //중간에 네트워크가 오래끊겨서 멀다면 순간이동이 더 자연스럽다.
                tr.position = currPos;
                tr.rotation = currRot;
            }
            else
            {
                tr.position = Vector3.Lerp(tr.position, currPos, Time.deltaTime * 10.0f);
                //네트워크 상태가 좋지 않더라도 Lerp를 이용해 부드럽게 이동
                tr.rotation = Quaternion.Slerp(tr.rotation, currRot, Time.deltaTime * 10.0f);
            }
        }

        micImage.enabled = photonVoiceView.IsSpeaking;
    }

    public void SetInfest()
    {
        Infest = true;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(tr.position);
            stream.SendNext(tr.rotation);
        }
        else
        {
            currPos = (Vector3)stream.ReceiveNext();
            currRot = (Quaternion)stream.ReceiveNext();
        }
    }
}
