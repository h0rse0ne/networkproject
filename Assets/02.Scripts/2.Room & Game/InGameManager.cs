﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class InGameManager : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI playerCountText;
    public TextMeshProUGUI roleText;
    public TextMeshProUGUI logText;
    public Transform gridTr;
    public GameObject msgText;
    public GameObject escapePanel;

    private GameObject[] players; 

    private void Awake()
    {
        GameManager.sceneState = 3;
    }

    // Use this for initialization
    void Start ()
    {
        // 커서 초기화 및 몇가지 변수 초기화
        GameManager.isEsc = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        CheckPlayerCount();
        SelectInfest();
    }
	
	// Update is called once per frame
	void Update ()
    {
        checkESC();
    }

    public void checkESC()
    {
        if (GameManager.isEsc == false)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            escapePanel.SetActive(false);
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            escapePanel.SetActive(true);
        }
    }

    void SelectInfest()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            players = GameObject.FindGameObjectsWithTag("PLAYER");
            GameObject inf = players[Random.Range(0, PhotonNetwork.PlayerList.Length)];
            Debug.Log(inf.GetComponent<PhotonView>().OwnerActorNr);
            photonView.RPC("SetInfestOther", RpcTarget.All, 
                inf.GetComponent<PhotonView>().OwnerActorNr);
        }
    }

    [PunRPC]
    void SetInfestOther(int actorNr)
    {
        if (GameManager.myPlayer.GetComponent<PhotonView>().OwnerActorNr
            == actorNr)
        {
            GameManager.isInfest = true;
            StartCoroutine(SendMsg("Game Started. You are Infest."));
            roleText.text = "Infest";
        }
        else
        {
            GameManager.isInfest = false;
            StartCoroutine(SendMsg("Game Started. You are People."));
            roleText.text = "People";
        }
        //obj.GetComponent<PlayerInit>().SetInfest();
    }



    IEnumerator SendMsg(string msg)
    {
        logText.text += msg + "\n";
        GameObject obj = Instantiate(msgText, gridTr);
        obj.GetComponent<TextMeshProUGUI>().text = msg;
        yield return new WaitForSeconds(3.0f);
        Destroy(obj);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        CheckPlayerCount();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        CheckPlayerCount();
    }

    public override void OnLeftRoom()
    {
        Destroy(GameObject.Find("Voice"));
        Destroy(GameObject.Find("GameManager"));
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("PLAYER"))
        {
            Destroy(obj);
        }

        SceneManager.LoadScene("1.Lobby");
    }

    public void CheckPlayerCount()
    {
        int currPlayer = PhotonNetwork.PlayerList.Length;
        int maxPlayer = PhotonNetwork.CurrentRoom.MaxPlayers;
        playerCountText.text = string.Format("[{0}/{1}]", currPlayer, maxPlayer);
    }


    public void yesClick()
    {
        PhotonNetwork.LeaveRoom();
    }

    public void noClick()
    {
        GameManager.isEsc = false;
    }
}
